package com.wayne.ble.scanner.sdk.datatypes;

import android.os.ParcelUuid;

import java.io.Serializable;

public class Nearal implements Serializable
{
	/**
	 * Created by fhd@Rabitech on 2/12/15.
	 */
	private static final long serialVersionUID = 1L;
	ParcelUuid[] parcelUuids;
	String address;
	int rssi;
	
	public Nearal( String address, int rssi) {
		
		this.address=address;
		this.rssi=rssi;
	}
	
	public String getAddress() 
	{
		return address;
	}	
	
	
	public int getRssi() {
		return rssi;
	}


}
