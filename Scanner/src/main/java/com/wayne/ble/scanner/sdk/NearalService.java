package com.wayne.ble.scanner.sdk;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import com.wayne.ble.scanner.R;
import com.wayne.ble.scanner.sdk.datatypes.Nearal;
import com.wayne.ble.scanner.sdk.datatypes.NearalList;

import java.util.ArrayList;

/**
 * Created by fhd@Rabitech  on 2/12/15.
 */
public  class NearalService extends Service implements NearalScanner.OnNearalRecievedListener
{
    static final String KEY_NearalData = "NearalData";
    static final String KEY_NearestNearalData = "NearesetNearalData";

	//-- Athul Raj
	//NotificationManager notificationManager = (NotificationManager)	getSystemService(NOTIFICATION_SERVICE);
	//-- Athul Raj

	public NearalScanner NearalScanner = null;
	private ArrayList<Nearal> nearalsArrayList;
	private Nearal nearest;

    @Override
    public IBinder onBind(Intent intent) 
    {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        NearalScanner = new NearalScanner(this);
        System.out.println("Service started");
        NearalScanner.setOnNearalRecievedListener(this);
        if(NearalScanner.hasBLEChip()) {
            NearalScanner.startBLEScanning();
        }else 
        {
            //sendBroadcast();
        }
        return START_STICKY;
    }

	@Override
	public void onRssiReset() {
		if(nearalsArrayList!=null)
		{
			new NearalList(nearalsArrayList);
			if(nearest!=null) {
				// for athul.... this is the nearest beacon detected (nearest)
				//	sendNearalBroadCast(com.rabitech.nearals.NearalsReciver.ACTTION_Brodcast_ON_Nearest_Nearal,KEY_NearestNearalData,nearest);
				nearest=null;
			}
		}
	}
	public void createNotification(Nearal object) {
		int rssi,power = 0;
		rssi = object.getRssi();

		if( rssi > -77 ) power = 44;
		if( rssi > -70 ) power = 55;
		if( rssi > -64 ) power = 65;
		if( rssi > -59 ) power = 74;
		if( rssi > -55 ) power = 82;
		if( rssi > -52 ) power = 89;
		if( rssi > -50 ) power = 95;
		if( rssi > -49 ) power = 100;
		//--Athul Raj--
		Intent intent = new Intent(this, NearalService.class);
		//PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);
		int val =
				Integer.parseInt(object.getAddress().substring(0,1), 16)   * 100 * 100 * 100 * 100 * 100 +
						Integer.parseInt(object.getAddress().substring(3,4), 16)   * 100 * 100 * 100 * 100 +
						Integer.parseInt(object.getAddress().substring(6,7), 16)   * 100 * 100 * 100 +
						Integer.parseInt(object.getAddress().substring(9,10), 16)  * 100 * 100 +
						Integer.parseInt(object.getAddress().substring(12,13), 16) * 100 +
						Integer.parseInt(object.getAddress().substring(15,16), 16);
		int img = R.drawable.a1;
		switch(val%4){
			case 0 :
				img = R.drawable.a1;
			break;
			case 1 :
				img = R.drawable.a2;
			break;
			case 2 :
				img = R.drawable.a3;
			break;
			case 3 :
				img = R.drawable.a4;
			break;
		}
		Notification n = new Notification.Builder(this)
				.setContentTitle("New offer, MAC : " + object.getAddress())
				.setContentText("Subject")
				.setSmallIcon(R.mipmap.ic_launcher)
				.setProgress(100, power, false)
				.setStyle(new Notification.BigPictureStyle()
						.bigPicture(BitmapFactory.decodeResource(getResources(), img)))
				.setSubText("Awesome Advert man.")
				//.setContentIntent(pIntent)
				.setAutoCancel(true).build();
				//.addAction(R.drawable.icon, "Call", pIntent)
				//.addAction(R.drawable.icon, "More", pIntent)
				//.addAction(R.drawable.icon, "And more", pIntent).build();
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(val, n);
		//--Athul Raj--
	}
	@Override
	public void onNearalRecieved(Nearal nearal) {

		//Toast.makeText(this,nearal.getAddress() +"@"+ nearal.getRssi(), Toast.LENGTH_SHORT).show();
		createNotification(nearal);
		if(nearalsArrayList==null)
			nearalsArrayList=new ArrayList<Nearal>();
		else
		{
			nearalsArrayList.add(nearal);
		}
		//sendNearalBroadCast(com.rabitech.nearals.NearalsReciver.ACTTION_Brodcast_ON_Nearal,KEY_NearalData,nearal);
		if(nearest==null)
			nearest=nearal;
		else if(nearal.getRssi()>nearest.getRssi())
		{
			nearest=nearal;
		}
		
	}

	private void sendNearalBroadCast(String broadcastTitle,String Intent_Extra_Key,Nearal... nearals) 
	{
		Intent bIntent = new Intent(broadcastTitle);
	    bIntent.putExtra(Intent_Extra_Key,nearals);
	      sendBroadcast(bIntent);
	}
}
