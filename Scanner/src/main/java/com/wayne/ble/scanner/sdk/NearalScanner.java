package com.wayne.ble.scanner.sdk;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanFilter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;

import com.wayne.ble.scanner.sdk.datatypes.Nearal;

import java.util.ArrayList;

/**
 * Created by fhd@Rabitech on 2/12/15.
 */
public class NearalScanner {
	public static final String nearalsUUID="B9407F30-F5F8-466E-AFF9-25556B57FE6D";// general Nearal uuid used for filtering BLE beacons(scan will find only BLEs under Nearals  )
    public BluetoothAdapter btAdapter = null;
    public Context oContext;
    private Handler oHandler = null;
 // Empty data
    byte[] manData = new byte[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    // Data Mask
    byte[] mask = new byte[]{0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0};
    public ArrayList<String> devices = null;
    private BluetoothAdapter.LeScanCallback oScanCallback = null;
    public BroadcastReceiver broadcastReceiver = null;
    private static final long SCAN_INTERVAL = 3500; // 3.5 seconds

    public NearalScanner(Context oContext)
    {

        if((oContext == null) || !(oContext instanceof Context)){
            throw new IllegalArgumentException("Please provide Context Instance");
        }
        this.oContext = oContext;
    }
    
    
    @SuppressLint("NewApi")
	private void enableBluetooth() 
    {
    	 final BluetoothManager bluetoothManager =
                 (BluetoothManager)oContext.getSystemService(Context.BLUETOOTH_SERVICE);
         btAdapter = bluetoothManager.getAdapter();
        if(btAdapter != null) {
            btAdapter.enable(); //enable bluetooth
        }
	}

	public interface OnNearalRecievedListener {
		public void onNearalRecieved(Nearal nearal);
		public void onRssiReset();
    }
    
    public OnNearalRecievedListener NearalRecievedListener = null;
	public void setOnNearalRecievedListener(OnNearalRecievedListener NearalRecievedListener){
    	this.NearalRecievedListener = NearalRecievedListener;
    }

    public boolean hasBLEChip(){
        PackageManager pm = oContext.getPackageManager();
        boolean feature_check = pm.hasSystemFeature("android.hardware.bluetooth_le");
        boolean version_check = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2);
        return  feature_check && version_check;
    }

    @SuppressLint("NewApi")
	public boolean startBLEScanning(){
        enableBluetooth();
        if(btAdapter == null){   // No Bluetooth ? :(
            return false;
        }
        if (!btAdapter.isEnabled()) {
			return false;
		}
        oHandler = new Handler();
        oScanCallback = new BluetoothAdapter.LeScanCallback() 
        {
           

			@Override
            public void onLeScan(final BluetoothDevice device, int rssi,
                                 byte[] scanRecord) {
            	if(device!=null)
            	{
                    onDevice(device.getUuids(),device.getAddress(),rssi);
                 //   System.out.println("UUID : -- "+device.getUuids());
//                    printScanRecord(scanRecord);
                   // System.out.println("UUID ==== "+UUID.randomUUID().toString());
            	}
            	else
            	{
            	}
            }
        };
        oHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                btAdapter.stopLeScan(oScanCallback);
                startNext();
            }
        }, SCAN_INTERVAL);
        // Copy UUID into data array and remove all "-"
        System.arraycopy(hexStringToByteArray(nearalsUUID.replace("-","")), 0, manData, 2, 16);
        new ScanFilter.Builder().setManufacturerData(76, manData, mask).build();
        boolean TEST = btAdapter.startLeScan(oScanCallback);
        Log.d("SCAN",TEST+"");
		return true;
    }
    
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    protected void onDevice(ParcelUuid[] parcelUuids, String address, int rssi) 
    {
    	Nearal nearal=new Nearal( address, rssi);
    	NearalRecievedListener.onNearalRecieved(nearal);		
	}
    public void startNext() {
          NearalRecievedListener.onRssiReset();
          //disableBluetooth();
          new Handler().postDelayed(new Runnable() {
              @Override
              public void run() {
                  new Handler().postDelayed(new Runnable() {
                      @Override
                      public void run() {
                          while (!startBLEScanning()) ;
                      }
                  }, 500);
                  //enableBluetooth();
              }
          }, 500);
      }
    @SuppressLint("NewApi")
	public void destroyGarbage(){
        btAdapter.stopLeScan(oScanCallback);
        btAdapter.cancelDiscovery();
        //oContext.unregisterReceiver(broadcastReceiver);
    }
    @SuppressLint("NewApi")
	private void disableBluetooth() 
	{
		 final BluetoothManager bluetoothManager =
                 (BluetoothManager)oContext.getSystemService(Context.BLUETOOTH_SERVICE);
         btAdapter = bluetoothManager.getAdapter();
         if(btAdapter != null) {
	            btAdapter.enable(); //enable bluetooth
	        }		
	}
}
