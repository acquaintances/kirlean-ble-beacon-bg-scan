package com.wayne.ble.scanner.sdk.interfaces;


import com.wayne.ble.scanner.sdk.datatypes.Nearal;
import com.wayne.ble.scanner.sdk.datatypes.NearalList;

/**
 * Created by fhd@Rabitech on 2/12/15.
 */
public interface NearalsBeaconListener 
{
	
	/**
	 * This function called back when a NearalBeacon is detected
	 *	 
	 * @param nearal object contain rssi,mac_id of the detected NearalBeacon
	 */
	void OnNearalDetected(Nearal nearal);
	/**
	 * This function called back when a scan is over.
	 *		 
	 * @param nearestNearal  object contain rssi,mac_id of the nearest(with higher rssi value)NearalBeacon detected so far in this scan.
	 * 
	 */

	void OnNearestNearal(Nearal nearestNearal);

	
	/**
	 * This function called back when a scan is over.
	 * 
	 * @param nearalList object contain an ArrayList 'nearls' which contains all the Nearal object corresponds to NearalBeacons detected so far in this scan.
	 */
	void OnNearalList(NearalList nearalList);

}
